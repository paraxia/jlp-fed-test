import Head from "next/head";
import Link from "next/link";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
// import { products } from "../mockData/data.json";
import axios from "axios";
import { useState, useEffect } from "react";

const Home = () => {

  const [products, setProducts] = useState([]);
  
  useEffect(() => {
    
    setProducts(products);
    
    axios
      .get("data.json")
      .then((res) => setProducts(res.data.products))
      .catch(err=>console.log(err))
  },[]);

  // Ran out of time integrating JEST
  // Tests for each product asset as they're loading from the API and being checked would be ideal etc
  // Any mismatch in database/api call would skip render of specific product and flag it
  // Would discuss with UX error handling in addition

  let items = products;
  let count = products.length;

  return (
    <main>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div className={styles.page}>
        <h1 className={styles.heading1}>
          Dishwashers ({count})
        </h1>
        
        {/* ran out of time but would add accessbility/ARIA attributes */}
        <section>
          <ul className={styles.grid}>
            {items && items.map((item) => (
              
              <li className={styles.item}>
                
                <Link
                  key={item.productId}
                  href={{
                    pathname: "/product-detail/[id]",
                    query: { id: item.productId ? item.productId : null  },
                  }}
                  as={`/product-detail/${item.productId}`}
                >
                  <a className={styles.link}>

                    <article className={styles.card}>
                        {/* // should be using next.js Image - having issues with local urls which was slowing me down here */}
                        <img src={item.image ? item.image : "placeholder"} alt={item.title} />
                        <div>
                          {item.title ? item.title : "We're sorry. There seems to be a problem finding this "}
                          <span className={styles.price}>
                              {item.price.now ? item.price.now : "Please contact one of our sales representatives" }
                          </span>
                        </div>
                    </article>

                  </a>
                </Link>
              </li>
            ))}
          </ul>
        </section>

      </div>
    </main>
  );
}


export default Home;

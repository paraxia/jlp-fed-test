import ProductCarousel from "../../components/product-carousel/product-carousel";
import { useRouter } from 'next/router'
import axios from "axios";
import { useState, useEffect } from "react";

const ProductDetail = () => {
  
  const router = useRouter();
  const productID = router.query;

  const [product, setProduct] = useState([]);

  useEffect(() => {
    setProduct(product);
    
    axios
      .get("data2.json")
      .then((res) => setProduct(res.data))
      .catch(err=>console.log(err))
  },[]);

  let data = product;

  // spent some time on this page but ran out of time after trying to resolve page rendering errors

  return (
    <div>
      <h1><div dangerouslySetInnerHTML={{ __html: data.title }} /></h1>
      <div>
        {productID}
        <ProductCarousel image={data.skus.media.images.urls[0]} />

        <h3>
          <h1>{productID}</h1>
          <div>{data.displaySpecialOffer}</div>
          <div>{data.additionalServices[includedServices]}</div>
        </h3>

        <div>
          <h3>Product information</h3>
        </div>
        <h3>Product specification</h3>
        <ul>
          {data.details.features[0].attributes.map((item) => (
            <li>
              <div><div dangerouslySetInnerHTML={{ __html: item.name }} /></div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default ProductDetail;
